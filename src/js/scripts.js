window.onload = function () {
    loadCarousel();
    loadSpons();
    loadcoffee();
};
//get coffee images

// get Sponser images
async function loadcoffee() {
    const data = await GetJsonData("coffee");
    displaycoffeImages(data)
}
function displaycoffeImages(data) {
    data.forEach(e => {
        const temp = document.querySelector(".flicktemp").content;
        const img = temp.cloneNode(true);
        const div = img.querySelector(".sponsimg")
        div.style.backgroundImage = "url(./img/gallery/" + e.id + ".jfif)"
        document.querySelector(".instafeed").appendChild(img);
    })
}

// get Sponser images
async function loadSpons() {
    const data = await GetJsonData("sponsers");
    displayImages(data)
}
function displayImages(data) {
    data.forEach(e => {
        const temp = document.querySelector(".sponsimTemp").content;
        const img = temp.cloneNode(true);
        const div = img.querySelector(".sponsimg")
        div.style.backgroundImage = "url(./img/spons/" + e.id + ".png)"
        document.querySelector(".sponstcontainer").appendChild(img);
    })
}

async function loadCarousel() {
    // Get src data from Json file 
    const data = await GetJsonData("imgsrcs");
    // make a loop for each to append the data
    let newdata = []
    data.forEach(e => {

        const torandom = {
            id: getRandomInt(data.length),
            src: e.src
        }
        newdata.push(torandom)
    })

    function compare(a, b) {
        const ValueA = a.id;
        const ValueB = b.id;
        let comparison = 0;
        if (ValueA > ValueB) {
            comparison = 1;
        } else if (ValueA < ValueB) {
            comparison = -1;
        }
        return comparison;
    }

    newdata.sort(compare).forEach(e => {
        // Get template content.
        const IMGTemp = document.querySelector(".IMGforCarousel").content;
        // Clone content.
        const clone = IMGTemp.cloneNode(true);
        // find img in template and change source for each
        const image = clone.querySelector("[data-img]")
        // Set image source
        image.setAttribute("src", "./img/" + e.src + "");
        // 
        if (e.id == 0) {
            image.parentElement.classList.add("active")
        }
        const carousel = document.querySelector(".carousel-inner");
        carousel.appendChild(clone);
    })
}

async function GetJsonData(what) {
    // Find json data from file as What is the parameter for choosing the file
    const response = await fetch(`./json/` + what + `.json`);
    const json = await response.json();
    return json;
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

//Load Articles to front page
window.addEventListener("DOMContentLoaded", Articles);

async function Articles(update) {
    // Get data from Article Json file 
    const data = await GetJsonData("articles");
    displayArticles(data, update);
}

// I've not made a paginator before and am not sure what the industry standard ways are, this is purely my guess at it it's working -- sort of when you reach the end the math starts
// to get messed up for some reason, I imagine this to be enough for this prototype at least.
// But there are probably many tools out there or different ways of making it work.

// set length of pages (how many)
let paginatorLenght = 0;
// set what page it is now
let paginatorPageis = 0;

// this func is activated by event listener listenButton() down below.
const pagevent = function () {

    if (this.dataset.btnpage == "back") {
        // minus by plus 3 to get 3 next articles
        paginatorPageis = paginatorPageis - 3;
        /// Run articles again to fetch the newest articles in json file and update the view.
        Articles("update");
    }
    if (this.dataset.btnpage == "forth") {
        // plus page by plus 3 to get 3 next articles,
        paginatorPageis = paginatorPageis + 3;
        /// Run articles again to fetch the newest articles in json file and update the view.
        Articles("update");
    }
}
// This function displayes the Article, it has two arguments, if which is update it will update (more in next line), otherwise it will run first iteration and give first 3 articles.
// If it's "update" then it will not run the first iteration but rather go throgh some if statements and increment some things.
function displayArticles(data, which) {
    // Clear the article section first.
    document.querySelector(".articlescontainer").innerHTML = "";
    // Get how many pages there are supposed to be.
    paginatorLenght = Math.ceil(data.length / 3);
    // First update statement.
    if (which == "update") {
        /// Display Page nr on page 
        document.querySelector(".whatPagis").textContent = paginatorPageis / 3;
    } else {
        /// Display first iteration of the Page nr on page (if not updated it's calculated differently).
        document.querySelector(".whatPagis").textContent = paginatorPageis;
    }
    // Run a loop through the data.
    data.forEach(e => {
        // Get template content.
        const IMGTemp = document.querySelector(".articletem").content;
        // Clone content.
        const clone = IMGTemp.cloneNode(true);
        //Create img template
        clone.querySelector(".imgcontainer").style.backgroundImage = "url(./img/" + e.img + ")"
        // Create Title temp and set text content
        clone.querySelector(".artiletitle").textContent = e.head;
        // Crete publish date template and set content
        clone.querySelector(".puplishdate").textContent = e.publish;
        // Crete articleheadline date template and set content
        clone.querySelector(".articleheadlinep").innerHTML += e.headline + `<span class="dots">...</span> <span class="more">more</span>`;
        // Get more
        clone.querySelector(".more").textContent = e.button;
        // Here I check what page it is, if it's equal to 4 then stop the paginator, and I also check if the individual article id's are between the paginator page as it is now and as it is 
        //if it would be the next.
        const checkpage = document.querySelector(".whatPagis").textContent;
        if (checkpage <= paginatorLenght && e.id >= paginatorPageis && e.id <= paginatorPageis + 2) {
            document.querySelector(".articlescontainer").appendChild(clone);
        }
    })
    listenButton()
}

const seemore = function () {
    if (this.dataset.BtnActive == "active") {
        this.dataset.BtnActive = "";
        const t = this.previousElementSibling;
        const more = t.querySelector(".more")
        const dots = t.querySelector(".dots")
        this.textContent = "Read More";
        more.style.display = "none";
        dots.style.display = "block";
    } else {
        const t = this.previousElementSibling;
        const more = t.querySelector(".more")
        const dots = t.querySelector(".dots")
        this.textContent = "Read Less";
        more.style.display = "block";
        dots.style.display = "none";

        this.dataset.BtnActive = "active";
    }
}

// here I'm adding event listeners to the buttons each time I append articles. Because I want to hide them if they reached the end or if its the start.
function listenButton() {
    // find both buttons.
    const btn = document.querySelectorAll("[data-btnPage]")
    //iterate throuh buttons.
    btn.forEach(e => {
        // if the forth button is equal or greater to paginatorLenght -1 then display none. This is where the math got a bit tricky because I originally planned this to be so that 
        // the last page is supposed to be = paginatorLenght, but something is up, I might come back to it.
        if (e.dataset.btnpage == "forth" && e.previousElementSibling.textContent == paginatorLenght - 1) {
            e.style.display = "none"
        } else {
            if (e.dataset.btnpage == "forth") {
                e.style.display = "block"
            }
        }
        // If back button is 0 display none.
        if (e.dataset.btnpage == "back" && e.nextElementSibling.textContent == 0) {
            e.style.display = "none"
        } else {
            if (e.dataset.btnpage == "back") {
                e.style.display = "block"
            }
        }
        // if clicked go all the way up almost and start the whole thing again.
        e.addEventListener("click", e = pagevent)
    });

    const readmore = document.querySelectorAll("[data-readmore]");
    readmore.forEach(e =>
        e.addEventListener("click", e = seemore)
    )
}

